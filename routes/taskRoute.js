// This document contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require ("../controllers/taskController.js");

router.get("/:id", (req,res) =>{

	taskController.getAllTask().then(result => res.send(result));
})
 

router.put("/:id/complete", (req,res) => { 
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


module.exports = router;
